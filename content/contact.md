+++
title = "Contact"
weight = 30
draft = false
+++

* ⭐Email: alejandros714@protonmail.com 

* Gitlab Profile: https://gitlab.com/shockrahwow

* Linkedin: https://www.linkedin.com/in/alejandro-santillana-38458b121/ 

* Blog: https://shockrah.neocities.org/

* Freelancing site: https://shockrah.shop
