+++
title = "Resume"
weight = 30
draft = false
+++

Alejandro Santillana - _alejandros714@protonmail.com_

## Education


In Progress - CSU Monterey Bay - Undergraduate B.A. in Computer Science
: 	Expected Graduation - December 2019


Finished	- Irvine Valley College - 2 years of lower division Undergraduate coursework

Graduated 	- Aliso Niguel High School - 2015

## Experience

Repair Technician - Loaves, Fishes, & Computers Inc.
: 	Diagnosed technical issues with the clientele's computers as well as performed computer repair. Responsibilities also included troubleshooting Windows when customers had issues.

School Project Lead - CSU Monterey Bay
: 	Led development of PHP back-end, front-end design, and database management. Also wrote documentation for other members of the team to use during their development work.
: 	https://scrimmersgg.herokuapp.com

## Projects (Links)

[Ripsy - ELF Parser](https://gitlab.com/shockrahwow/hexd)
:	A tool written in C for statically analyzing ELF binaries on Linux

[Twitch Chat Keeper](https://gitlab.com/shockrahwow/twitchwatch)
:	Monitors twitch chat discussions with some fun chat features written in Python3.7
	
[Personal Blog](https://gitlab.com/shockrahwow/shockrah-city)
:	Blog website built and maintained through hand rolled Bash scripts

## Skills 

Programming Languages
: 	C, Assembly, Python3, Bash

Misc Skills
: 	Git, Linux, Windows troubleshooting, Linux troubleshooting

