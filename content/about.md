+++
title = "About me"
weight = 30
draft = false
+++

_*Currently searching for a software developer position to start in the summmer of 2019. Last semester of college will be from August to December 2019._

I have a great passion for libre software and low level systems programming, specifically on Unix systems like those part of a GNU/Linux distribution.

My short term focus as a learner changes from time to time, given that right now it's learning how malware/spyware works.
This still doesn't change my goal of learing how something like the Linux kernel works.

Unrelated to to the previous I also post randomly on a [blog](https://gitlab.com/shockrahwow/shockrah-city) where I post about things which happen to interest me at a given moment.
